package main

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestGetFact(t *testing.T) {
	want := "cats have nine lives"

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		_ = json.NewEncoder(w).Encode(map[string]string{
			"fact": want,
		})
	}))

	repo := newFactRepository(s.URL)
	got, err := repo.getFact(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	if got != want {
		t.Errorf("Unexpected fact returned. Got %q, want %q", got, want)
	}
}

type myFakeService func(*http.Request) (*http.Response, error)

func (s myFakeService) RoundTrip(req *http.Request) (*http.Response, error) {
	return s(req)
}

func TestGetFact2(t *testing.T) {
	client := &http.Client{
		Transport: myFakeService(func(*http.Request) (*http.Response, error) {
			return &http.Response{
				StatusCode: http.StatusInternalServerError,
				Header: http.Header{
					"Content-Type": []string{"application/json"},
				},
				Body: io.NopCloser(strings.NewReader(`{"fact":"cats have nine lives"}`)),
			}, nil
		}),
	}

	repo := &factRepository{
		client: client,
	}

	got, err := repo.getFact(context.Background())
	if err != nil {
		t.Fatal(err)
	}
	want := "cats have nine lives"
	if got != want {
		t.Errorf("Unexpected fact returned. Want %q, got %q", want, got)
	}
}
