package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

func main() {
	repo := newFactRepository("https://catfact.ninja/fact")
	fact, err := repo.getFact(context.Background())
	if err != nil {
		panic(err)
	}
	fmt.Println(fact)
}

type factRepository struct {
	address string
	client  *http.Client
}

func newFactRepository(addr string) *factRepository {
	return &factRepository{
		address: addr,
		client:  http.DefaultClient,
	}
}

type factResponse struct {
	Fact string `json:"fact"`
}

func (r *factRepository) getFact(ctx context.Context) (string, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, r.address, nil)
	if err != nil {
		return "", err
	}
	res, err := r.client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	var result factResponse
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return "", err
	}
	return result.Fact, nil
}
